import argparse
import datetime
import os
import requests


gitlab_url = "https://gitlab.com/"
group_id = os.getenv("GROUP_ID")


def get_headers():
    # Set the headers for the API request
    personal_access_token = os.getenv("TOKEN")
    headers = {
        "Private-Token": personal_access_token
    }
    return headers


def get_user_id(username):
    headers = get_headers()
    url = f"{gitlab_url}api/v4/users?username={username}"

    response = requests.get(
        url,
        headers=headers
    )

    if response.status_code != 200:
        raise Exception("Error getting issues")

    users = response.json()

    for user in users:
        return user.get("id")


def get_paginated_issues(url_function):
    headers = get_headers()
    # Make a GET request to get a list of all the issues in the project

    page = 1
    total_pages = 10
    issues = []

    while page < total_pages:

        response = requests.get(
            url_function(page),
            headers=headers
        )

        response_headers = response.headers

        if response.status_code != 200:
            raise Exception("Error getting issues")

        if not response.json():
            break

        # Convert the response to a JSON object
        issues.extend(response.json())

        if "x-total-pages" in response_headers:
            total_pages = int(response_headers.get("x-total-pages")) + 1

        print(f"Got {page} of {total_pages} ")
        # page = int(response_headers.get("x-next-page"))

        page += 1
        print(f"Next is {page} of {total_pages} ")

    return issues


def get_milestone_issues(milestone, confidential="true"):
    labels = "group::pipeline%20execution"

    def get_url(page):
        return (f"{gitlab_url}api/v4/groups/9970/issues?labels={labels}"
                f"&milestone={milestone}&state=closed&confidential={confidential}&scope=all&page={page}")

    return get_paginated_issues(
        get_url
    )


def get_ci_data_partitioning_epics(confidential="true"):
    labels = "CI%20data%20partitioning"

    def get_url(page):
        return f"{gitlab_url}api/v4/groups/9970/epics?labels={labels}&confidential={confidential}&page={page}"

    return get_paginated_issues(
        get_url
    )


def get_ci_data_partitioning_issues(confidential="true"):
    labels = "CI%20data%20partitioning"

    def get_url(page):
        return f"{gitlab_url}api/v4/groups/9970/issues?labels={labels}&confidential={confidential}&scope=all&page={page}"

    return get_paginated_issues(
        get_url
    )


def get_mrs(author=None, reviewer=None, approver_id=None, start_date=None, end_date=None):
    api_url = f"{gitlab_url}api/v4//merge_requests?"
    params = ["scope=all", "confidential=true"]
    if author:
        params.append(f"author_username={author}")
    if reviewer:
        params.append(f"reviewer_username={reviewer}")
    if approver_id:
        params.append(f"approved_by_ids[]={approver_id}")
    if start_date:
        params.append(f"created_after={start_date}")
    if end_date:
        params.append(f"created_before={end_date}")

    api_url += "&".join(params)

    def get_url(page):
        return f"{api_url}&page={page}"

    return get_paginated_issues(
        get_url
    )


if __name__ == '__main__':
    # create_daily_issues()
    get_milestone_issues("16.4")
