import csv
from helper import get_mrs, get_user_id
from pprint import pprint

team_members = {"tianwenchen", "mfanGitLab", "jivanvl", "hfyngvason", "pburdette", "allison.browne", "mbobin",
                    "drew", "panoskanell", "carolinesimpson"}

team_member_ids = {}

if __name__ == '__main__':

    stats = {}

    for team_member in team_members:

        print(f"Getting Authored MRs for {team_member}")
        authored_mrs = get_mrs(author=team_member, start_date="2022-11-01T08:00:00Z", end_date="2023-10-31T08:00:00Z")

        user_id = get_user_id(team_member)

        print(f"Getting Approver MRs for {team_member} - {user_id}")
        reviewed_mrs = get_mrs(approver_id=user_id, start_date="2022-11-01T08:00:00Z", end_date="2023-10-31T08:00:00Z")

        stats[team_member] = {
            "authored": len(authored_mrs),
            "reviewed": len(reviewed_mrs),
        }

    pprint(stats)
