import csv
from helper import get_milestone_issues

if __name__ == '__main__':
    # "15.8", "15.9", "15.10", "15.11",
    milestones = ["16.0", "16.1", "16.2", "16.3", "16.4"]
    team_members = {"tianwenchen", "mfanGitLab", "jivanvl", "hfyngvason", "pburdette", "allison.browne", "mbobin",
                    "drew", "panoskanell"}
    with open('issues.csv', 'w', newline='') as csvfile:

        issue_csv = csv.writer(csvfile)
        issue_csv.writerow(["Milestone", "ID", "assignee", "Title", "Link", "Deliverable", "Stretch", "Ignore",
                            "FixedOutsideTeam", "ClosedNoFix", "Maintenance", "Bug", "Feature",
                            "CIDataPartitioning",  "UnaccountedFor", "Security", "Infradev", "frontend",
                            "FeatureFlagRollout",
                            "RequiredForDeliverable", "Notes", "Labels"])

        for milestone in milestones:
            for flag in ["true", "false"]:
                issues = get_milestone_issues(milestone, flag)

                for issue in issues:
                    assignees = set([x.get("username") for x in issue.get("assignees")])
                    assignees_string = ", ".join(assignees)
                    deliverable = "Deliverable" in issue.get("labels") or ""
                    stretch = "Stretch" in issue.get("labels") or ""
                    ignore = "type::ignore" in issue.get("labels") or ""

                    fixed_outside_team = assignees.intersection(team_members) == set() or ""
                    fixed_by_intersection = assignees.intersection(team_members)
                    closed_no_fix = "closed::cannot reproduce" in issue.get("labels") or ""
                    ci_data_partitioning = "CI data partitioning" in issue.get("labels") or ""
                    frontend = "frontend" in issue.get("labels") or ""

                    feature_flag = "feature flag" in issue.get("labels") or ""
                    maintenance = "type::maintenance" in issue.get("labels") or ""
                    bug = "type::bug" in issue.get("labels") or ""
                    feature = "type::feature" in issue.get("labels") or ""
                    security = "security::vulnerability" in issue.get("labels") or ""
                    infradev = "infradev" in issue.get("labels") or ""

                    unaccounted_for = not (
                            deliverable or fixed_outside_team or closed_no_fix or ci_data_partitioning or frontend
                    ) or ""

                    issue_csv.writerow([milestone, issue.get("id"), assignees_string, issue.get("title"),
                                        issue.get("web_url"), deliverable, stretch, ignore, fixed_outside_team,
                                        closed_no_fix,
                                        maintenance, bug, feature, ci_data_partitioning, unaccounted_for,
                                        security, infradev, frontend, feature_flag, "", "",
                                        issue.get("labels"), fixed_by_intersection])
