import csv
from helper import get_ci_data_partitioning_issues, get_ci_data_partitioning_epics


team_members = {"tianwenchen", "mfanGitLab", "jivanvl", "hfyngvason", "pburdette", "allison.browne", "mbobin",
                    "drew", "panoskanell"}


def write_epics_to_csv(filename):
    with open(filename, 'w', newline='') as csvfile:

        issue_csv = csv.writer(csvfile)
        header_written = False
        item_headers = []

        for flag in ["true", "false"]:
            epics = get_ci_data_partitioning_epics(flag)

            if epics:
                if not header_written:
                    # item_headers = list(epics[0].keys())
                    item_headers = ["iid", "parent_iid", "title", "start_date", "end_date", "state", "web_url"]
                    # item_headers.remove('description')
                    issue_csv.writerow(
                        item_headers)
                    header_written = True

                for epic in epics:
                    issue_csv.writerow(
                        [epic.get(x) for x in item_headers]
                    )
                #
                # milestone = issue.get("milestone") or ""
                # if milestone:
                #     milestone = milestone.get("title")
                # state = issue.get("state") or ""
                # assignees = set([x.get("username") for x in issue.get("assignees")])
                # assignees_string = ", ".join(assignees)
                #
                # epic_json = issue.get("epic") or ""
                # if epic_json:
                #     epic = epic_json.get("title")
                #     epic_url = epic_json.get("url")
                # deliverable = "Deliverable" in issue.get("labels") or ""
                # stretch = "Stretch" in issue.get("labels") or ""
                # ignore = "type::ignore" in issue.get("labels") or ""
                #
                # fixed_outside_team = assignees.intersection(team_members) == set() or ""
                # fixed_by_intersection = assignees.intersection(team_members)
                # closed_no_fix = "closed::cannot reproduce" in issue.get("labels") or ""
                # ci_data_partitioning = "CI data partitioning" in issue.get("labels") or ""
                # frontend = "frontend" in issue.get("labels") or ""
                #
                # feature_flag = "feature flag" in issue.get("labels") or ""
                # maintenance = "type::maintenance" in issue.get("labels") or ""
                # bug = "type::bug" in issue.get("labels") or ""
                # feature = "type::feature" in issue.get("labels") or ""
                # security = "security::vulnerability" in issue.get("labels") or ""
                # infradev = "infradev" in issue.get("labels") or ""
                #
                # unaccounted_for = not (
                #         deliverable or fixed_outside_team or closed_no_fix or ci_data_partitioning or frontend
                # ) or ""

                # issue_csv.writerow(
                #     [milestone, state, issue.get("id"), epic, epic_url, assignees_string, issue.get("title"),
                #      issue.get("web_url"), deliverable, stretch, ignore, fixed_outside_team,
                #      closed_no_fix,
                #      maintenance, bug, feature, ci_data_partitioning, unaccounted_for,
                #      security, infradev, frontend, feature_flag, "", "",
                #      issue.get("labels"), fixed_by_intersection])


def write_issues_to_csv(filename):
    with open(filename, 'w', newline='') as csvfile:

        issue_csv = csv.writer(csvfile)
        issue_csv.writerow(
            ["Milestone", "State", "ID", "Epic", "EpicURL", "assignee", "Title", "Link", "Deliverable", "Stretch",
             "Ignore",
             "FixedOutsideTeam", "ClosedNoFix", "Maintenance", "Bug", "Feature",
             "CIDataPartitioning", "UnaccountedFor", "Security", "Infradev", "frontend",
             "FeatureFlagRollout",
             "RequiredForDeliverable", "Notes", "Labels"])

        for flag in ["true", "false"]:
            issues = get_ci_data_partitioning_issues(flag)

            for issue in issues:
                milestone = issue.get("milestone") or ""
                if milestone:
                    milestone = milestone.get("title")
                state = issue.get("state") or ""
                assignees = set([x.get("username") for x in issue.get("assignees")])
                assignees_string = ", ".join(assignees)

                epic_json = issue.get("epic") or ""
                if epic_json:
                    epic = epic_json.get("title")
                    epic_url = epic_json.get("url")
                deliverable = "Deliverable" in issue.get("labels") or ""
                stretch = "Stretch" in issue.get("labels") or ""
                ignore = "type::ignore" in issue.get("labels") or ""

                fixed_outside_team = assignees.intersection(team_members) == set() or ""
                fixed_by_intersection = assignees.intersection(team_members)
                closed_no_fix = "closed::cannot reproduce" in issue.get("labels") or ""
                ci_data_partitioning = "CI data partitioning" in issue.get("labels") or ""
                frontend = "frontend" in issue.get("labels") or ""

                feature_flag = "feature flag" in issue.get("labels") or ""
                maintenance = "type::maintenance" in issue.get("labels") or ""
                bug = "type::bug" in issue.get("labels") or ""
                feature = "type::feature" in issue.get("labels") or ""
                security = "security::vulnerability" in issue.get("labels") or ""
                infradev = "infradev" in issue.get("labels") or ""

                unaccounted_for = not (
                        deliverable or fixed_outside_team or closed_no_fix or ci_data_partitioning or frontend
                ) or ""

                issue_csv.writerow(
                    [milestone, state, issue.get("id"), epic, epic_url, assignees_string, issue.get("title"),
                     issue.get("web_url"), deliverable, stretch, ignore, fixed_outside_team,
                     closed_no_fix,
                     maintenance, bug, feature, ci_data_partitioning, unaccounted_for,
                     security, infradev, frontend, feature_flag, "", "",
                     issue.get("labels"), fixed_by_intersection])


if __name__ == '__main__':
    write_epics_to_csv("cdp-epics.csv")
    # write_issues_to_csv("cdp-issues.csv")


